$(document).ready(function(){
	// Click on Next Section Arrow Button at Slideshow
	$('.next-arrow-wrapper').click(function(){
		event.preventDefault();
		var next_section = $('.main-content').children('.shopify-section').eq(1);
		var header_height  = $('.site-header').height();
		var offset = $(next_section).offset().top - header_height;
            
        $('body,html').animate({
            scrollTop: offset
        }, 1000);

	})
})